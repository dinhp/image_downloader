package com.carleton.dinhpblakej.image_downloader;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

/**
 * Created by Phuong Dinh on 4/7/15.
 */

public class DeleteActivity extends ActionBarActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);


        Button button = (Button)this.findViewById(R.id.download_button);
        button.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {

        String[] files = getApplicationContext().fileList();
        //get all files and delete them all
        for (String file : files) {
            String dir = getFilesDir().getAbsolutePath();
            File deleteFile = new File(dir, file);
            boolean deleteSucceed = deleteFile.delete();
            Log.i("Delete Check", "File deleted here " + dir + file + deleteSucceed);
        }

        Toast toast = Toast.makeText(this, "Mooooo, everything is deleted!", Toast.LENGTH_SHORT);
        toast.show();

        final MediaPlayer cow_moo = new MediaPlayer();
        //MUSIC GOES HERE!!!
        if(cow_moo.isPlaying())//if the sound is already played and got clicked again
        {
            cow_moo.stop();
        }
        try {
            cow_moo.reset();
            AssetFileDescriptor afd = getAssets().openFd("Cow_Moo.mp3");
            cow_moo.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            cow_moo.prepare();
            cow_moo.start();
            Log.i("Sound Check", "Moooooo");
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
