package com.carleton.dinhpblakej.image_downloader;

/**
 * Downloader.java
 * Jeff Ondich, 22 May 2014
 * Copyright (c) 2014 Ultralingua, Inc. All rights reserved.
 *
 * *** MODIFIED 9 May 2014 by John Blake ***
 *
 * When Downloader's download method is invoked, the Downloader spawns a
 * new thread, tries to download the file to the specified destination file,
 * and posts Message objects to the caller's Handler.
 *
 * If the destination file is null, the downloader will accumulate the downloaded
 * file in a String, and report it via the HANDLE_ONE_FILE_CONTENTS_SUCCESS message
 * instead of the HANDLE_ONE_FILE_SUCCESS message used when the downloaded content
 * is saved in a file.
 *
 * Here are Message.what values sent by Downloader, along with the values of
 * Message.arg1, arg2, and obj for each type of message.
 *
 * HANDLE_STARTING_ONE_FILE: arg1 is the index of the file being started, arg2 is the total number of files
 * HANDLE_SET_PROGRESS_MAX: arg1 is the new maximum value
 * HANDLE_PROGRESS: arg1 is the new progress value, arg2 is the maximum value
 * HANDLE_ONE_FILE_SUCCESS: obj is the File object of the destination file
 * HANDLE_ONE_FILE_CONTENTS_SUCCESS obj is the String contents of the downloaded URL
 * HANDLE_ALL_FILES_SUCCESS: obj is the ArrayList&lt;Downloadable&gt; of downloadables.
 * HANDLE_ERROR: obj is the thrown Exception object
 * HANDLE_TIMEOUT: obj is the File object of the destination file
 *
 */

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class Downloader {
    private ArrayList<Downloadable> downloadables;
    private HashMap<String, String> requestHeaders;

    private Handler downloaderUpdatesHandler;

    private boolean timeoutHasOccurred;
    private int timeoutInMillis;
    private Handler timeoutHandler;
    private Runnable timeoutRunnable;

    public static final int HANDLE_SET_PROGRESS_MAX = 0x7162;
    public static final int HANDLE_STARTING_ONE_FILE = 0x7163;
    public static final int HANDLE_PROGRESS = 0x7164;
    public static final int HANDLE_ONE_FILE_SUCCESS = 0x7165;
    public static final int HANDLE_ONE_FILE_CONTENTS_SUCCESS = 0x7166;
    public static final int HANDLE_ALL_FILES_SUCCESS = 0x7167;
    public static final int HANDLE_ERROR = 0x7168;
    public static final int HANDLE_TIMEOUT = 0x7169;

    public class Downloadable {
        public String sourceURL;
        public File destinationFile;

        public Downloadable(String url, File destination) {
            this.sourceURL = url;
            this.destinationFile = destination;
        }
    }

    public Downloader() {
        this.timeoutHandler = new Handler();
        this.timeoutRunnable = new Runnable() {
            public void run() {
                timeoutHasOccurred = true;
            }
        };
        this.downloadables = new ArrayList<Downloadable>();
    }

    public void addFileToDownload(String url, File destination) {
        this.downloadables.add(new Downloadable(url, destination));
    }

    public void download(int timeoutInSeconds,
                         HashMap<String, String> requestHeaders,
                         Handler handler)
    {
        this.requestHeaders = requestHeaders;
        this.timeoutInMillis = 1000 * timeoutInSeconds;
        this.downloaderUpdatesHandler = handler;

        Thread thread = new Thread(new DownloadRunnable());
        thread.setDaemon(true);

        this.timeoutHandler.postDelayed(this.timeoutRunnable, this.timeoutInMillis);

        thread.start();
    }

    private void publishStartOneFile(int indexOfThisFile, int numberOfFilesToDownload) {
        Message message = new Message();
        message.arg1 = indexOfThisFile;
        message.arg2 = numberOfFilesToDownload;
        message.what = Downloader.HANDLE_STARTING_ONE_FILE;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishProgressMax(int progressBarMax) {
        Message message = new Message();
        message.arg1 = progressBarMax;
        message.what = Downloader.HANDLE_SET_PROGRESS_MAX;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishProgress(int progressBarProgress, int progressBarMax) {
        Message message = new Message();
        message.arg1 = progressBarProgress;
        message.arg2 = progressBarMax;
        message.what = Downloader.HANDLE_PROGRESS;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishTimeout(Downloadable downloadable) {
        Message message = new Message();
        message.what = Downloader.HANDLE_TIMEOUT;
        message.obj = downloadable.destinationFile;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishError(Exception e) {
        Message message = new Message();
        message.obj = e;
        message.what = Downloader.HANDLE_ERROR;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishOneFileSuccess(Downloadable downloadable) {
        Message message = new Message();
        message.what = Downloader.HANDLE_ONE_FILE_SUCCESS;
        message.obj = downloadable.destinationFile;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishOneFileSuccessWithContents(Downloadable downloadable, String contents) {
        Message message = new Message();
        message.what = Downloader.HANDLE_ONE_FILE_CONTENTS_SUCCESS;
        message.obj = contents;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private void publishAllFilesSuccess() {
        Message message = new Message();
        String path = this.downloadables.get(0).destinationFile.getAbsolutePath();
        if (path.contains("/cache/")){
            downloadables = null;
        }
        message.what = Downloader.HANDLE_ALL_FILES_SUCCESS;
        message.obj = this.downloadables;
        this.downloaderUpdatesHandler.sendMessage(message);
    }

    private class DownloadRunnable implements Runnable {
        public void run() {
            try {
                int indexOfThisFile = 1;
                int numberOfFilesToDownload = downloadables.size();
                for (Downloadable downloadable : downloadables) {
                    if (!timeoutHasOccurred) {
                        publishStartOneFile(indexOfThisFile, numberOfFilesToDownload);
                        this.doDownload(downloadable);
                    }else{
                        Log.d("TIMEOUT", "download has timed out");
                    }
                    indexOfThisFile++;
                }
                publishAllFilesSuccess();

            } catch (IOException e) {
                publishError(e);
            }
        }

        private void doDownload(Downloadable downloadable) throws IOException {
            URL url = new URL(downloadable.sourceURL);
            URLConnection connection = url.openConnection();
            for (String key : requestHeaders.keySet()) {
                connection.setRequestProperty(key, requestHeaders.get(key));
            }
            connection.connect();
            int progressFileLength = connection.getContentLength();
            int progressBarMax = (progressFileLength > 0 ? progressFileLength : 100);
            publishProgressMax(progressBarMax);
            int progressBarProgress = 0;
            publishProgress(progressBarProgress, progressBarMax);

            InputStream inputStream = new BufferedInputStream(connection.getInputStream(), 12582924);
            OutputStream outputStream;
            if (downloadable.destinationFile != null) {
                outputStream = new FileOutputStream(downloadable.destinationFile);
            } else {
                outputStream = new ByteArrayOutputStream(12582924);
            }

            int byteCount;
            byte data[] = new byte[12582924];
            while (!timeoutHasOccurred && (byteCount = inputStream.read(data)) != -1) {
                outputStream.write(data, 0, byteCount);
                timeoutHandler.removeCallbacks(timeoutRunnable);
                timeoutHandler.postDelayed(timeoutRunnable, timeoutInMillis);
                progressBarProgress += byteCount;
                publishProgress(progressBarProgress, progressBarMax);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();

            if (timeoutHasOccurred) {
                publishTimeout(downloadable);
            } else if (downloadable.destinationFile != null) {
                publishOneFileSuccess(downloadable);
            } else {
                publishOneFileSuccessWithContents(downloadable, outputStream.toString());
            }
        }
    }
}
