package com.carleton.dinhpblakej.image_downloader;

import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        //get message (file directory) from parent activity
        Bundle extrafile = getIntent().getExtras();
        String file_directory;
        String fileNameReplaced;
        if (extrafile!=null){
            file_directory = extrafile.getString("file_directory");
            //Set name of image
            fileNameReplaced = "File opened: "+ file_directory.replace("/data/data/com.carleton.dinhpblakej.image_downloader/files/","");
        }
        else{
            file_directory = "@mipmap/cow";
            fileNameReplaced = "Sorry, file could not be opened. Enjoy this picture instead";
        }
        Log.i("file directory:", file_directory);

        //Open the image file
        TextView fileName = (TextView) findViewById(R.id.fileName);
        fileName.setText(fileNameReplaced);
        ImageView imageView = (ImageView) findViewById(R.id.image_source);
        imageView.setImageBitmap(BitmapFactory.decodeFile(file_directory));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
