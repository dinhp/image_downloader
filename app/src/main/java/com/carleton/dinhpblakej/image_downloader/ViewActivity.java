package com.carleton.dinhpblakej.image_downloader;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by Phuong Dinh on 4/6/15.
 */

public class ViewActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        ArrayList<String> listItems = new ArrayList<String>();
        String[] files = getApplicationContext().fileList();
        for (String file : files) if (!file.equals("HTML")) listItems.add(file);//add in files which are not html



        ListAdapter adapter = new ListAdapter(this, R.layout.list_cell2, listItems);
        ListView listView2 = (ListView) this.findViewById(android.R.id.list);
        listView2.setAdapter(adapter);
        //Make a view listener to go to New View Activity here
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                                TextView textView = (TextView) v.findViewById(R.id.listCellTextView);
                                                if (v != null && textView != null) {
                                                    //Write intent to new activity here,
                                                    //Pass to new activity name of the file -> hence display the right image
                                                    String message = getApplicationContext().getFilesDir().toString()+ "/"+textView.getText();
                                                    Log.i("File directory!!!", message);

                                                    Intent intent = new Intent(ViewActivity.this, ImageActivity.class);
                                                    //Send message to child activity
                                                    intent.putExtra("file_directory",message);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

                                                    }
                                                }
                                            }

        );


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}