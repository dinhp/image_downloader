package com.carleton.dinhpblakej.image_downloader;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Phuong Dinh on 4/6/15.
 */

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Put a logo for fun (there is a logo at the Menu too! They are by the way different )
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.moose);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        //Initiate Array List
        ArrayList<String> listItems = new ArrayList<String>();
        listItems.add("Google New Image");
        listItems.add("View Old Images");
        listItems.add("Delete All Images");
        ListAdapter adapter = new ListAdapter(this, R.layout.list_cell, listItems);
        ListView listView = (ListView) this.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        //Upon clicking one of the list activity
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                                TextView textView = (TextView) v.findViewById(R.id.listCellTextView);
                                                if (v != null && textView != null) {
                                                    //Write intent to new activity here by textView.getText()
                                                    if (textView.getText() == "Google New Image") {
                                                        Intent intent = new Intent(MainActivity.this, DownloadActivity.class);
                                                        startActivity(intent);
                                                    }else if (textView.getText() == "View Old Images"){
                                                        Intent intent = new Intent(MainActivity.this, ViewActivity.class);
                                                        startActivity(intent);
                                                    }else if (textView.getText() == "Delete All Images"){
                                                        Intent intent = new Intent(MainActivity.this, DeleteActivity.class);
                                                        startActivity(intent);
                                                    }
                                                    overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
                                                }

                                            }
                                        }
        );

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //new MenuInflater(getApplication()).inflate(R.menu.menu_main, menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

    //Action bar control
        if (id == R.id.action_About) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }else if (id == R.id.action_mainMenu) {
            Toast toast = Toast.makeText(this, "This is here for fun! \nActually, to demonstrate Toast!", Toast.LENGTH_LONG);
            toast.show();

        }

        return super.onOptionsItemSelected(item);
    }


}
