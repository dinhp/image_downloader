package com.carleton.dinhpblakej.image_downloader;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Phuong Dinh and John Blake on 4/7/15.
 * Edited from Jeff's Download example
 * Changed to google the first image of a keyword instead -> hence download a JSON -> parse JSON -> download image
 */

public class DownloadActivity extends ActionBarActivity implements View.OnClickListener{
    private final String HTML_DIRECTORY_NAME = "html";
    private final int DOWNLOAD_TIMEOUT_SECONDS = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        Button button = (Button)this.findViewById(R.id.download_button);
        button.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        EditText fileNameTextField = (EditText)this.findViewById(R.id.file_name_edit_text);
        String searchTerm = fileNameTextField.getText().toString();//get Search field
        FileDownloadHandler handler = new FileDownloadHandler();
        FileDownloadHandler handler1 = new FileDownloadHandler();
        this.downloadFiles(searchTerm, handler, handler1);
    }

    public String[] getImageURL(String searchTerm, Handler htmlDownloadHandler){
        Log.d("LOCATION", "Inside getImageURL");
        try{
            searchTerm = searchTerm.replaceAll("\\s", "\\%20").toLowerCase();
        }catch(Exception e){
            Log.e("DOWNLOAD_DEMO", "searchTerm cleaning failed");
        }
        String[] output = new String[3];
        File pageHTML = new File(this.getApplicationContext().getCacheDir(), HTML_DIRECTORY_NAME);
        if (!pageHTML.exists()) {
            if (!pageHTML.mkdir()){
                Log.e("DOWNLOAD_DEMO", "pageHTML.mkdir failed");
                return output;
            }
        }


        File destination = new File(pageHTML, searchTerm);
        String sourceURL = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + searchTerm;//get the JSON string of the google search
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("User-Agent", "Phuong-John-Image-Download");
        Downloader downloader = new Downloader();
        downloader.addFileToDownload(sourceURL, destination);
        downloader.download(DOWNLOAD_TIMEOUT_SECONDS, requestHeaders, htmlDownloadHandler);

        Boolean fileExists = false;
        while (!fileExists){
            if (destination.exists()){
                String fileName = destination.toString();
                Log.d("FILE_REPORT", fileName);
                fileExists = true;
            }
        }

        StringBuilder htmlBuilder = new StringBuilder();

        try {
            Scanner scanner = new Scanner(destination);
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();
                htmlBuilder.append(line);
            }
        } catch (FileNotFoundException e) {
            Log.d("HTML", "File not found");
        }

        String payload = htmlBuilder.toString();
        Log.d("HTML: ", payload);

        String expression = "url.:.(https?://[-A-Za-z0-9+@/_!.;%]*)";//get the url of the first image
        Pattern pattern = Pattern.compile(expression, Pattern.DOTALL | Pattern.UNIX_LINES);
        Matcher m = pattern.matcher(payload);
        Boolean b = m.matches();

        String imageURL = b.toString();
        while (m.find()){

            imageURL = m.group(1);
            break;
        }
        Log.d("URL_RESULT", imageURL);

        File[] cache = getApplicationContext().getCacheDir().listFiles();
        for (File html : cache) {
            File[] cacheFiles = html.listFiles();
            for (File file : cacheFiles){
                String location = file.getAbsolutePath();
                boolean deleteSucceed = file.delete();
                Log.i("Delete Check", "File deleted here " + location + deleteSucceed);
            }
        }
        output = parseFileNameFromURL(imageURL);
        return output;
    }

    public String[] parseFileNameFromURL(String url){
        String[] output = new String[3];
        String[] temp = url.split("/");
        String extensionExpression = "\\.(.{3,4})$";
        Pattern urlPattern = Pattern.compile(extensionExpression, Pattern.MULTILINE);
        Matcher matcher = urlPattern.matcher(url);
        Boolean bool = matcher.matches();
        String imageExtension = bool.toString();
        while (matcher.find()){
            Log.d("URL", "Getting File Extension");
            imageExtension = "." + matcher.group(1);
        }
        output[0] = url;
        output[1] = temp[temp.length-1];
        output[2] = imageExtension;

        return output;

    }

    public void downloadFiles(String searchTerm, Handler downloadHandler, Handler htmlDownloadHandler) {//Edited based on Jeff's downloadFiles example
        Log.d("LOCATION", "Inside download files");
        String path = this.getApplicationContext().getFilesDir().toString();

        this.hideKeyboard();
        String[] imageData = getImageURL(searchTerm, htmlDownloadHandler);
        String imageURL = imageData[0];
        String fileExtension = imageData[2];
        File destination = new File(path, searchTerm + fileExtension);//Save file as searchTerm instead of fileName


        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("User-Agent", "Phuong-John-Image-Downloader");

        Downloader downloader = new Downloader();
        downloader.addFileToDownload(imageURL, destination);//get the image
        downloader.download(DOWNLOAD_TIMEOUT_SECONDS, requestHeaders, downloadHandler);
    }

    public void hideKeyboard() {
        EditText myEditText = (EditText)this.findViewById(R.id.file_name_edit_text);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    private class FileDownloadHandler extends Handler {//Credit to Jeff Ondich
        @Override
        public void handleMessage(Message message) {
            File file;
            String s;
            ProgressBar progressBar;

            switch (message.what) {
                case Downloader.HANDLE_STARTING_ONE_FILE:
                    progressBar = (ProgressBar)findViewById(R.id.progress_bar);
                    progressBar.setMax(100);
                    progressBar.setProgress(0);
                    break;

                case Downloader.HANDLE_PROGRESS:
                    progressBar = (ProgressBar)findViewById(R.id.progress_bar);
                    int progressPercentage = message.arg2 == 0 ? 0 : (100 * message.arg1) / message.arg2;
                    progressBar.setProgress(progressPercentage);
                    break;

                case Downloader.HANDLE_ALL_FILES_SUCCESS:
                    ArrayList<Downloader.Downloadable> downloadables = (ArrayList<Downloader.Downloadable>) message.obj;
                    if (downloadables != null && downloadables.size() > 0) {
                        ImageView imageView = (ImageView)findViewById(R.id.image_view);
                        Downloader.Downloadable downloadable = downloadables.get(0);
                        Uri uri = Uri.fromFile(downloadable.destinationFile);
                        imageView.setImageURI(uri);
                    }
                    break;

                case Downloader.HANDLE_TIMEOUT:
                    file = (File)message.obj;
                    s = (file != null ? file.getName() : "??");
                    Log.d("DOWNLOAD_DEMO", "Download timed out for " + s);
                    break;

                case Downloader.HANDLE_ERROR:
                    Exception e = (Exception)message.obj;
                    Log.d("DOWNLOAD_DEMO", "Downloading error: " + e);
                    if (e.toString().contains("java.io.FileNotFoundException")){
                        FileDownloadHandler handler = new FileDownloadHandler();
                        FileDownloadHandler handler1 = new FileDownloadHandler();
                        downloadFiles("404", handler, handler1);
                    }
                    break;
            }
            super.handleMessage(message);
        }

    }
}
